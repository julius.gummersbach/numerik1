import math


def get_A(n):
    return [2 for i in range(n)], [-1 for i in range(n - 1)]


def special_cholesky(diag1, diag2):
    n = len(diag1)
    for i in range(n - 1):
        diag1[i] = math.sqrt(diag1[i])
        diag2[i] /= diag1[i]
        diag1[i + 1] -= diag2[i] ** 2
    diag1[n - 1] = math.sqrt(diag1[n - 1])
    return diag1, diag2


print(special_cholesky(get_A(100)[0], get_A(100)[1]))
print(special_cholesky(get_A(1000)[0], get_A(1000)[1]))
print(special_cholesky(get_A(10000)[0], get_A(10000)[1]))
