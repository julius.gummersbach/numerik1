"""
LU[row][column]
( 1  5  0 )
( 4  9  7 )  = [[1,5,0],[4,9,7],[2,0,1]] 
( 2  0  1 )
"""


def solveFromLU(LU, p, b):
    c = permutation(p, b)
    y = vorwaerts(LU, c)
    x = rueckwaerts(LU, y)
    return x


def solve(A, b):
    LU, p = zerlegung(A)
    return solveFromLU(LU, p, b)


def zerlegung(A):
    dim = len(A)
    p = []
    for diag in range(dim):
        if A[diag][diag] == 0 and diag != dim - 1:
            found_next = False
            swap = -1
            for i in range(diag + 1, dim):
                if A[i][diag] != 0:
                    swap = i
                    found_next = True
                    break
            if not found_next:
                raise RuntimeError("Given Matrix invalid: No non-null entry in column ", diag, " below diagonal")
            p.append(swap + 1)
            temp = A[diag]
            A[diag] = A[swap]
            A[swap] = temp
        elif diag != dim - 1:
            p.append(diag + 1)
        for row in range(diag + 1, dim):
            A[row][diag] /= A[diag][diag]
            for column in range(diag + 1, dim):
                A[row][column] -= A[row][diag] * A[diag][column]
    return A, p


# Codierung von p: jeder Eintrag gibt an mit welcher Zeile die aktuelle Zeile getauscht werden soll. (zählen beginnend bei 1)
# Das wird nacheinander abgearbeitet
def permutation(p, b):
    for i in range(len(p)):
        temp = b[i]
        b[i] = b[p[i] - 1]
        b[p[i] - 1] = temp
    return b


def vorwaerts(LU, c):
    x = []
    for row in range(len(LU)):
        sum_left = 0
        for column in range(row):
            sum_left += LU[row][column] * x[column]
        res = (c[row] - sum_left)
        x.append(res)
    return x


def rueckwaerts(LU, y):
    dim = len(LU)
    x = [0 for i in range(dim)]
    for row in reversed(range(len(LU))):
        sum_right = 0
        for column in range(row + 1, dim):
            sum_right += LU[row][column] * x[column]
        res = (y[row] - sum_right) / LU[row][row]
        x[row] = res
    return x


def getProblem(n):
    A = []
    b = []
    for i in range(n):
        A.append([])
        b.append(1 / (i + 2))  # i+1 + 1
        for j in range(n):
            A[i].append(1 / (i + j + 1))  # i+1 + j+1 -1
    return A, b


if __name__ == '__main__':
    print(permutation([2, 2, 4], [1, 2, 3, 4]))
    print(permutation([2], [[1, 0, 0], [0, 1, 0], [0, 0, 1]]))
    print("vorwaerts: (1,2,3): ", vorwaerts([[98, 2, 1], [0, 9, 4], [0, 0, 12]], [1, 2, 3]))
    print("vorwaerts: (1,-4,12): ", vorwaerts([[2, 0, 0], [6, 2, 0], [7, 4, 5]], [1, 2, 3]))
    print("rueckwaerts: (-9,1,1): ", rueckwaerts([[1, 2, 8], [0, 3, 1], [0, 0, 2]], [1, 4, 2]))

    print(zerlegung([[0, 0, 0, 1], [2, 1, 2, 0], [4, 4, 0, 0], [2, 3, 1, 0]]))
    print(solve([[0, 0, 0, 1], [2, 1, 2, 0], [4, 4, 0, 0], [2, 3, 1, 0]], [3, 5, 4, 5]))
    print(solve([[0, 0, 0, 1], [2, 1, 2, 0], [4, 4, 0, 0], [2, 3, 1, 0]], [4, 10, 12, 11]))

    print(solve(getProblem(10)[0], getProblem(10)[1]))
    print(solve(getProblem(20)[0], getProblem(20)[1]))
    print(solve(getProblem(100)[0], getProblem(100)[1]))
