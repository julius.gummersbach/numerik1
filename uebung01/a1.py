import math


def standard_pq_formel(p, q):
    if p * p + q < 0:
        return "no roots"
    sqroot = math.sqrt(p * p + q)
    x1 = -p - sqroot
    x2 = -p + sqroot
    return x2 if x2 > x1 else x1


def adv_pq_formel(p, q):
    if p * p + q < 0:
        return "no roots"
    x1 = -p - math.sqrt(p * p + q)
    x2 = -q / x1
    return x2 if x2 > x1 else x1


print(standard_pq_formel(2, 1))
print(adv_pq_formel(2, 1))
print(standard_pq_formel(-2, 3))
print(adv_pq_formel(-2, 3))

print("-----------")
print(standard_pq_formel(100, 1))
print(adv_pq_formel(100, 1))
print(standard_pq_formel(10000, 1))
print(adv_pq_formel(10000, 1))
print(standard_pq_formel(1000000, 1))
print(adv_pq_formel(1000000, 1))
print(standard_pq_formel(10000000, 1))
print(adv_pq_formel(10000000, 1))
print(standard_pq_formel(100000000, 1))
print(adv_pq_formel(100000000, 1))

# adv pq formel ist für große Zahlenwerte genauer
