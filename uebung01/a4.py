import math

import numpy


def trapezregel(f, a, b, n):
    res = 0
    h = (b - a) / n
    points = []
    for i in range(1, n):
        points.append(a + i * h)
    s = numpy.array([f(x) for x in points]).sum()
    return h / 2 * (f(a) + 2 * s + f(b))


print(trapezregel(lambda x: math.log(x), 1, 2, 100))
