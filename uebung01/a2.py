import math


def exponentialreihe1(n, x):
    res = 0
    for k in range(n + 1):
        res += x ** k / math.factorial(k)
    return res


def exponentialreihe2(n, x):
    res = 0
    for k in range(n + 1):
        res += x ** (n - k) / math.factorial(n - k)
    return res


print(exponentialreihe1(10, 4))
print(exponentialreihe2(10, 4))
print(exponentialreihe1(100, 4))
print(exponentialreihe2(100, 4))
print(exponentialreihe1(1000, 4))
print(exponentialreihe2(1000, 4))
print(math.exp(4))

print(exponentialreihe1(10, 12))
print(exponentialreihe2(10, 12))
print(exponentialreihe1(100, 12))
print(exponentialreihe2(100, 12))
print(exponentialreihe1(1000, 12))
print(exponentialreihe2(1000, 12))
print(math.exp(12))
