import math


def rechtecksumme(f, a, b, n):
    res = 0
    h = (b - a) / n
    for i in range(n):
        res += f(a + i * h)
    return res * h


def trapezregel(f, a, b, n):
    res = 0
    h = (b - a) / n
    for i in range(1, n):
        res += f(a + i * h)
    return h / 2 * (f(a) + 2 * res + f(b))


print(rechtecksumme(lambda x: math.log(x), 1, 2, 100))
print(trapezregel(lambda x: math.log(x), 1, 2, 100))
