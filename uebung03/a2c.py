import numpy as np

import uebung02.a2


def sherman_morrison(LU, p, Adach, u, vT, bDach):
    z = uebung02.a2.solveFromLU(LU, p, u)
    if 1 + np.matmul(vT, z) == 0:
        raise AttributeError("Given Matrix ", Adach, " is not regular")
    alpha = 1 / (1 + np.matmul(vT, z))
    zDach = uebung02.a2.solveFromLU(LU, p, bDach)
    factor = alpha * np.matmul(vT, zDach)
    return [zDach[i] - factor * z[i] for i in range(len(z))]


if __name__ == '__main__':
    A = [[0, 0, 0, 1], [2, 1, 2, 0], [4, 4, 0, 0], [2, 3, 1, 0]]
    u = [0, 1, 2, 3]
    vT = [0, 0, 0, 1]
    bDach = [3, 5, 4, 5]

    LU, p = uebung02.a2.zerlegung(A)
    Adach = np.matmul([u], [[vT[i]] for i in range(len(vT))])

    print(sherman_morrison(LU, p, Adach, u, vT, bDach))
