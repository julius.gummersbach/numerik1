import math

import uebung02.a2


def solve(A, b):
    LU, p = zerlegung(A)
    return uebung02.a2.solveFromLU(LU, p, b)


def zerlegung(A):
    dim = len(A)
    p = []
    for diag in range(dim):
        if diag != dim - 1:
            swap = diag
            for i in range(diag + 1, dim):
                if math.fabs(A[i][diag]) > math.fabs(A[swap][diag]):
                    swap = i
            p.append(swap + 1)
            temp = A[diag]
            A[diag] = A[swap]
            A[swap] = temp
        elif diag != dim - 1:
            p.append(diag + 1)
        for row in range(diag + 1, dim):
            A[row][diag] /= A[diag][diag]
            for column in range(diag + 1, dim):
                A[row][column] -= A[row][diag] * A[diag][column]
    return A, p


def getProblem(n, beta=10):
    A = [[0 for i in range(n)] for i in range(n)]
    A[0][0] = 1
    A[0][n-1] = beta
    for i in range(n-1):
        A[i][i] = 1
        A[i+1][i] = -beta

    b = [1 + beta]
    for i in range(n-2):
        b.append(1-beta)
    b.append(-beta)
    return A, b


if __name__ == '__main__':
    print(zerlegung([[0, 0, 0, 1], [2, 1, 2, 0], [4, 4, 0, 0], [2, 3, 1, 0]]))
    print(solve([[0, 0, 0, 1], [2, 1, 2, 0], [4, 4, 0, 0], [2, 3, 1, 0]], [3, 5, 4, 5]))
    print(solve([[0, 0, 0, 1], [2, 1, 2, 0], [4, 4, 0, 0], [2, 3, 1, 0]], [4, 10, 12, 11]))

    print(uebung02.a2.solve(getProblem(10)[0], getProblem(10)[1]))
    print(solve(getProblem(10)[0], getProblem(10)[1]))
    print(uebung02.a2.solve(getProblem(20)[0], getProblem(20)[1]))
    print(solve(getProblem(20)[0], getProblem(20)[1]))
    print(uebung02.a2.solve(getProblem(100)[0], getProblem(100)[1]))
    print(solve(getProblem(100)[0], getProblem(100)[1]))
